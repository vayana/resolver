from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tables import CodeMapping,Constraint,DeclarativeBase


engine = create_engine('mysql://pluto:pluto@localhost/codes', echo=True)
Session = sessionmaker(bind=engine)
db_session = Session()


def build_code_mapping(entity,counterparty,code,constraints=list()):
    cm = CodeMapping(entity=entity, counterparty=counterparty, code=code)
    for (name, value) in constraints:
        cm.constraints.append(Constraint( identifier_name=name,identifier_value=value))
    return cm


def create_code_mappings():
    DeclarativeBase.metadata.drop_all(bind=engine)
    DeclarativeBase.metadata.create_all(bind=engine, checkfirst=False)
    code_mappings = list()
    #Dealer Financing
    code_mappings.append(build_code_mapping("relpetro","dealerr","Rel_dealerR"))
    code_mappings.append(build_code_mapping("relpetro", "dealerr", "dealerrel"))
    code_mappings.append(build_code_mapping("dealerr", "relpetro", "dealerrel"))
    code_mappings.append(build_code_mapping("dealerr", "relpetro", "Reliance Petrolium"))
    code_mappings.append(build_code_mapping("relpetro", "bob", "bobrp"))
    code_mappings.append(build_code_mapping("relpetro", "bob", "Bank of Baroda"))
    code_mappings.append(build_code_mapping("bob", "relpetro", "BOBRel"))
    code_mappings.append(build_code_mapping("bob", "dealerr", "Bob_DealerR",constraints= [('plant_code','p01'),('related_entity', 'relpetro')]))
    #payables
    code_mappings.append(build_code_mapping("future", "dummy", "V001"))
    code_mappings.append(build_code_mapping("future", "dummy", "V002"))
    code_mappings.append(build_code_mapping("future", "dummy", "V003"))
    code_mappings.append(build_code_mapping("bob", "dummy", "WCH001",constraints= [('enterprise_identifier','V001')]))
    code_mappings.append(build_code_mapping("bob", "dummy", "WCH0023",constraints= [('enterprise_identifier','V002'),('enterprise_identifier','V003'),
                                                                                    ('related_entid','future'),('plant_code','P001')]))
    code_mappings.append(build_code_mapping("bob", "dummy", "WCH0024", constraints=[('related_entid', 'future'),('plant_code', 'P001')]))
    code_mappings.append(build_code_mapping("bob", "dummy", "WCH0025", constraints=[('related_entid', 'kecint'), ('plant_code', 'P001')]))


    for cm in code_mappings:
        db_session.add(cm)
    db_session.commit()

    #print(code_mapping1)