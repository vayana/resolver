import os
import unittest

from pluto.integration.reversefeeds.name_generators import relpetro_reverse_feed_name
from pluto.integration.reversefeeds.rev_feed_generators import write_to_rev_output_file
from hamcrest import greater_than
from hamcrest.core.assert_that import assert_that
from hamcrest.core.core.allof import all_of
from hamcrest.core.core import is_,not_

from pluto.scripts.utils import initialize_environment
from pluto.utils.file_utils import build_and_create_full_local_path

from resolver import get_code,get_counterparty

class DealerFinancing(unittest.TestCase):


    def test_get_counterparty_by_seller_or_interested_cpty(self):

        assert_that(get_counterparty("relpetro","Rel_dealerR"),not_(None))
        assert_that(get_counterparty("relpetro", "dealerrel"), not_(None))
        assert_that(get_counterparty("relpetro", "notexist"), is_(None))
        assert_that(get_counterparty("relpetro", "bobrp"), not_(None))

    def test_get_counterparty_by_buyer_or_borrower(self):
        assert_that(get_counterparty("dealerr","dealerrel"),not_(None))
        assert_that(get_counterparty("dealerr", "Reliance Petrolium"), not_(None))

    def test_get_bank_code(self):
        assert_that(get_code("bob","relpetro"),not_(None))
        assert_that(get_code("bob", "dealerr"), not_(None))





