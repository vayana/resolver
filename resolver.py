from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

from intial_setup import Session,create_code_mappings
from tables import CodeMapping,Constraint
from sqlalchemy import and_

db_session = Session()


def get_counterparty(entity, code):
    # we expect the code/name to be unique for a entity so it will only be for single counterparty
    try:
        code_mapping_result = db_session.query(CodeMapping).filter(and_(CodeMapping.entity == entity,
                                                              CodeMapping.code == code,
                                                              )).one()
        print(code_mapping_result)
        return code_mapping_result
    except NoResultFound:
        print(' No code mapping is defined for {} by code {}'.format(entity, code))
        return None
    except MultipleResultsFound:
        print('Weird!!!!!! - bank nad code uni que constraint violated')
        return None

def get_code(entity, counterparty, context=None):
   if context is None or any(context) is False:
   #   we expect only single code mappping
       try :
            code_mapping_result = db_session.query(CodeMapping).filter(and_(CodeMapping.entity == entity,
                                                 CodeMapping.counterparty == counterparty ,
                                                 )).one()
            return code_mapping_result
       except NoResultFound:
           print(' No code mapping is defined for {} to identify {}'.format(entity,counterparty))
           return None
       except MultipleResultsFound:
           print('Multiple Code mappings are defined for {} to identify {}.More filters are required'.format(entity, counterparty))
           return None
   else:

       if isinstance(context,dict) is False:
            print('constraints must be in dict format')
            return None
       names = [k for k,_ in context.items()]
       values = [v for _, v in context.items()]
       code_mappings = db_session.query(CodeMapping).join(Constraint).filter(and_(CodeMapping.entity == entity,
                                                                       CodeMapping.counterparty == counterparty,
                                                                       Constraint.identifier_name.in_(names),
                                                                       Constraint.identifier_value.in_(values))).all()

       # if with subset of identifier a single code is mapped then is it okay to return???

       if len(code_mappings)>1:
           print('Multiple Code maappings are defined for {} to identify {} with filters {}.More filters are required'.format(entity,
                                                                                                              counterparty,context))
           return None


if __name__ == '__main__':
    create_code_mappings()
    # print(any({}))
    # print(any({1:0}))
    get_code("bob", "dummy", context={'plant_code': 'P001'})



# data class CodeMapping(val entity: PrincipalId,
#                        val counterparty: PrincipalId,
#                        val relatedEntity: PrincipalId?,
#                        val counterpartyCode: String,
#                        val constraints: List<Pair<AttributeId,String>>) {
#     companion object {
#         fun resolve(entity: PrincipalId, otherEntity: PrincipalId, thirdEntity: PrincipalId,
#                     context: List<Pair<AttributeId, String>>) : Either<Fault,String> =
#                 TODO()
#         fun resolve(entity: PrincipalId, thirdEntity: PrincipalId?, code: String,
#                     context: List<Pair<AttributeId, String>>) : Either<Fault,PrincipalId> =
#             TODO()
#     }
# }
