from sqlalchemy import Column, Integer, String, types, ForeignKey, func, orm, UniqueConstraint
from pluto.model.base import  bigint
from sqlalchemy.ext.declarative import declarative_base

class VayanaBase(object):
    """This base class shall be used as a template to create
    a DeclarativeBase class for the model classes. It defines
    a generic __repr__() method that will build a string
    representation of the model objects using all the attributes
    and their values."""
    def __repr__(self):
        L = []
        for k in self.__class__.__table__.c.keys():
            value = getattr(self, k, '')
            L.append("%s=%r" % (k, value))
        return '%s(%s)' % (self.__class__.__name__, ','.join(L))

# Assign the same metadata object we created earlier
DeclarativeBase = declarative_base(cls=VayanaBase)

class CodeMapping(DeclarativeBase):
    __tablename__= "code_mappings"
    __table_args__ = (UniqueConstraint('entity', 'code', name='uq_entity_code'),
        {"mysql_engine": "InnoDB",
        "mysql_row_format": "DYNAMIC",
        "mysql_charset": "utf8" })
    id = Column(bigint, primary_key=True,autoincrement=True)
    entity = Column(String(100))
    counterparty = Column(String(100))
    #related_entity = Column(String(100),nullable=True,default=None)
    code = Column(String(100))


class Constraint(DeclarativeBase):
    __tablename__ = "constraints"
    __table_args__ = (UniqueConstraint('code_mapping_id', 'identifier_name','identifier_value'),{
        "mysql_engine": "InnoDB",
        "mysql_row_format": "DYNAMIC",
        "mysql_charset": "utf8"
    })

    id = Column(bigint, autoincrement=True, primary_key=True)
    code_mapping_id = Column(bigint, ForeignKey(CodeMapping.id, name="fk_code_mapping_id"), nullable=False)
    identifier_name = Column(types.String(50), nullable=False)
    identifier_value = Column(types.String(50), nullable=False)


CodeMapping.constraints = orm.relation(Constraint,
                                          primaryjoin=Constraint.code_mapping_id == CodeMapping.id,
                                          uselist=True, backref="code_mapping")
